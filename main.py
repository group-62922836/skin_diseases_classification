# main.py
from fastapi import FastAPI, File, UploadFile, Request, Form
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles  # Import the StaticFiles module
from io import BytesIO
import numpy as np
from PIL import Image
import tensorflow as tf

app = FastAPI(
    max_upload_size=10000000  # Set the maximum upload size in bytes
)

# Use tensorflow.keras.models.load_model instead of tensorflow.keras.load_model
MODEL = tf.keras.models.load_model("saved_model/2")

CLASS_NAMES = [
    'Atopic Dermatitis',
    'Basal Cell Carcinoma (BCC)',
    'Benign Keratosis-like Lesions (BKL)',
    'Eczema',
    'Melanocytic Nevi (NV)',
    'Melanoma',
    'No Skin Disease',
    'Psoriasis pictures Lichen Planus and related diseases',
    'Seborrheic Keratoses and other Benign Tumors',
    'Tinea Ringworm Candidiasis and other Fungal Infections',
    'Warts Molluscum and other Viral Infections'
]

# Define the path for the static files
app.mount("/static", StaticFiles(directory="../static"), name="static")

templates = Jinja2Templates(directory="../templates")

@app.get("/", response_class=HTMLResponse)
async def home(request: Request):
    return templates.TemplateResponse("home.html", {"request": request})

def read_file_as_image(data) -> np.ndarray:
    # Open the image and convert to RGB mode
    img = Image.open(BytesIO(data)).convert('RGB')

    # Resize the image to the target size, e.g., (256, 256)
    target_size = (256, 256)
    img = img.resize(target_size)

    # Convert the image to a numpy array
    image = np.array(img)

    return image

@app.post("/predict")
async def predict(request: Request, file: UploadFile = File(...)):
    print("Received request for prediction.")

    image = read_file_as_image(await file.read())
    print("Image read successfully.")

    img_batch = np.expand_dims(image, axis=0)

    # Assuming you want to use the loaded model for prediction
    prediction_result = MODEL.predict(img_batch)

    print("Prediction successful.")

    predicted_class_index = np.argmax(prediction_result[0]).item()
    predicted_class_name = CLASS_NAMES[predicted_class_index]
    confidence = float(np.max(prediction_result[0]))

    return templates.TemplateResponse("res.html", {
        "request": request,
        "class_index": predicted_class_index,
        "class_name": predicted_class_name,
        "confidence": confidence
    }, media_type="text/html")
