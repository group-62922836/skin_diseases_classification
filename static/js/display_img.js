function predict() {
    // Get the selected image file
    var fileInput = document.getElementById('imageInput');
    var file = fileInput.files[0];

    // Create FormData object to send the file to the server
    var formData = new FormData();
    formData.append('file', file);

    // Send a POST request to the server
    fetch('/predict', {
        method: 'POST',
        body: formData
    })
    .then(response => response.json())
    .then(data => {
        // Update the page with the prediction result
        var resultDiv = document.getElementById('predictionResult');
        resultDiv.innerHTML = `<p>Class Index: ${data.class_index}</p>
                               <p>Class Name: ${data.class_name}</p>
                               <p>Confidence: ${data.confidence}</p>`;
    })
    .catch(error => console.error('Error:', error));
}
